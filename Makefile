.PHONY: build serve tags clean

HUGO = hugo $(1) || nix-shell --run "hugo $(1)"

build:
	$(call HUGO)

serve:
	$(call HUGO,server)

clean:
	rm -rf public
