---
title: Frittata
tags: [ prova ]
tempo: 5 min
dose: 1 persona
---

## Ingredienti
- 2 uova

Opzionalmente:
- mais
- cipolla
- melanzane
- erbe:
  * urtizzôns (cime di luppolo)
  * grisolòn (silene rigonfia)
  * prezzemolo
  * origano
  * ortiche

## Procedimento
1. Scaldare un filo d'olio di oliva in una piccola padella
2. Inserire le uova, il sale fino ed eventuali altri ingredienti
3. Mescolare spesso

