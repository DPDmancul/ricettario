---
title: Minestrone
tags: [ prova, prova2 ]
tempo: 1:30
dose: 4 litri
---

## Ingredienti
- 4 patate
- 2 carote
- 2 zucchine (o zucca)
- 1 cipolla
- 1 gambo di sedano

Opzionalmente:
- Concentrato di pomodoro

## Procedimento
1. Pulire, pelare e tagliare a pezzettoni le verdure
2. Riempire la pentola di acqua fredda ed inserire le verdure
3. Portare a bollore
4. Aggiungere sale grosso e olio di oliva
5. Far bollire almeno per un'ora
6. Pestare le verdure

## Consigli
- Se si usano verdure congelate, inserirle quando bolle
